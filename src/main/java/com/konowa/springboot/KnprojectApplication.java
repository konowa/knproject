package com.konowa.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(KnprojectApplication.class, args);
	}

}
